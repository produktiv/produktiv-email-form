<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\SMTP;
    
    require 'libs/Exception.php';
    require 'libs/PHPMailer.php';
    require 'libs/SMTP.php';

    class ProduktvEmailForm {
        public $options;
        private $mail; 

        function __construct($options) {
            $this->options = $options;
            $this->mail = new PHPMailer(true);
        }

        public function sendEmail() {
            try {
                $this->setSmtpSettings();
                $this->setRecipients();
                $this->setEmailContent();
                $this->mail->send();
                echo json_encode(array("status" => true, "msg" => "Email sent"));
            } catch (Exception $e) {
                echo json_encode(array("status" => false, "msg" => "Email is not sent"));
            }
            
        }

        public function setSmtpSettings() {
            //Server settings
            $this->mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $this->mail->isSMTP();                                            // Send using SMTP
            $this->mail->Host       = $this->options['smtp_host'];            // Set the SMTP server to send through
            $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $this->mail->Username   = $this->options['smtp_username'];        // SMTP username
            $this->mail->Password   = $this->options['smtp_pw'];              // SMTP password
            $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $this->mail->Port = 587;                                          // TCP port to connect to
        }

        public function setRecipients() {
            //Recipients
            $this->mail->setFrom($this->options['from_email'], $this->options['from_name']);
            $this->mail->addAddress($this->options['to_email'], $this->options['to_name']);     // Add a recipient            
            $this->mail->addReplyTo($this->options['from_email'], $this->options['from_name']);            
        }

        public function setEmailContent() {
            // Content
            $this->mail->isHTML(true);                                  // Set email format to HTML
            $this->mail->Subject = $this->options['email_subject'];
            $this->mail->Body    = $this->options['email_body'];
        }
    }
?>