<?php 
    require_once 'src/ProduktvEmailForm.php';
    
    $emailTest = new \ProduktvEmailForm\ProduktvEmailForm(
        array( 
            "smtp_host"=>'', 
            "smtp_username"=> "",
            "smtp_pw"=> "",
            "from_email" => "",
            "from_name" => "",
            "to_email" => "",
            "to_name" => "",
            "email_subject" => "Test Email",
            "email_body" => "Test Body <br/> Test"
        )
    );
        
    echo $emailTest->sendEmail();
?>